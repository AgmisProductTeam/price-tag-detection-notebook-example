# README #

### This repository is meant to help set up AWS Marketplace Price Tag detection model

For this reason it has methods for:
- loading model to instance
- preparing payload for request
- sending request
- displaying image with recognized price tags